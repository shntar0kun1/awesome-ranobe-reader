import datetime
import os
import shutil
from collections import OrderedDict
import gi
from urllib.request import pathname2url

from slugify import slugify
from modules.settings_manager import Settings
from modules.yandex_tts import YandexTTS

gi.require_version('Gtk', '3.0')
gi.require_version('Gst', '1.0')
from gi.repository import Gtk, Gst, GLib

Gst.init(None)


def filepath2url(filepath):
    return 'file://' + pathname2url(filepath)


class AwesomeReader:
    def __init__(self):
        self.tts = None
        self.playlist_dict = None
        self.settings_data = Settings.load_settings()

        self.builder = Gtk.Builder()
        self.builder.add_from_file('ui.glade')
        self.builder.connect_signals(self)

        self.voice = self.builder.get_object('voice')
        self.api_token = self.builder.get_object('api_token')
        self.folder_id = self.builder.get_object('folder_id')
        self.lang = self.builder.get_object('lang')
        self.root_folder = self.builder.get_object('root_folder')

        self.novel_selector = self.builder.get_object('novel_selector')
        self.novel_name = self.builder.get_object('novel_name')
        self.part_name = self.builder.get_object('part_name')
        self.text = self.builder.get_object('main_text')

        self.playlist = self.builder.get_object('playlist')
        self.playing_now = self.builder.get_object('playing_now')
        self.volume_button = self.builder.get_object('volume_button')
        self.progress_bar = self.builder.get_object('progress_bar')
        self.player = Gst.ElementFactory.make('playbin', 'playbin')

        GLib.timeout_add_seconds(1, self.progress_bar_updater)

        self.edit_novel_dialog = self.builder.get_object('edit_novel_dialog')
        self.edit_novel_entry = self.builder.get_object('edit_novel_entry')

        self.window = self.builder.get_object('window')
        self.window.show()

    @staticmethod
    def _slugify(string):
        return slugify(string, to_lower=True, separator='_')

    def _reload_settings(self):
        self.settings_data = Settings.load_settings()

        if self.settings_data is not None:
            self.voice.set_text(self.settings_data['voice'])
            self.api_token.set_text(self.settings_data['api_token'])
            self.folder_id.set_text(self.settings_data['folder_id'])
            self.lang.set_text(self.settings_data['lang'])
            self.root_folder.set_filename(self.settings_data['root_folder'])

    def _get_novels_list(self):
        if self.settings_data is None:
            return []

        return [x[0] for x in os.walk(self.settings_data['root_folder'])]

    def _reload_novels_list(self):
        self.novel_selector.remove_all()

        if self.settings_data is not None:
            novels_list = os.listdir(self.settings_data['root_folder'])

            for novel_name in novels_list:
                self.novel_selector.append(str(novels_list.index(novel_name)), novel_name)

    def on_novel_selector_changed(self, obj, data=None):
        self.load_playlist(self.novel_name.get_text())

    def on_edit_novel_button_clicked(self, obj, data=None):
        self.edit_novel_dialog.show_all()

    def on_cancel_editing(self, obj, data=None):
        self.edit_novel_dialog.hide()

    def on_add_button_clicked(self, obj, data=None):
        new_novel_name = self._slugify(self.edit_novel_entry.get_text())

        if new_novel_name not in self._get_novels_list():
            folder_path = os.path.join(self.settings_data['root_folder'], new_novel_name)
            os.makedirs(folder_path, exist_ok=True)
            self._reload_novels_list()
            self.load_playlist(new_novel_name)
            self.novel_name.set_text(new_novel_name)

        self.edit_novel_dialog.hide()

    def on_delete_button_clicked(self, obj, data=None):
        novel_name = self._slugify(self.edit_novel_entry.get_text())
        folder_path = os.path.join(self.settings_data['root_folder'], novel_name)

        if folder_path in self._get_novels_list():
            shutil.rmtree(folder_path)
            self._reload_novels_list()
            self.novel_name.set_text('')

        self.edit_novel_dialog.hide()

    def on_progress_bar_format_value(self, obj, value, data=None):
        result = str(datetime.timedelta(seconds=value/1e9)).split('.')[0]
        return result

    def on_progress_bar_change_value(self, obj, value, time=0):
        self.player.seek_simple(
            Gst.Format.TIME,
            Gst.SeekFlags.FLUSH,
            int(time)
        )

    def progress_bar_updater(self, *args, **kwargs):
        position = self.get_current_position()
        self.progress_bar.set_value(position)
        return True

    def on_save_settings_button_clicked(self, obj, data=None):
        settings_data = {
            'voice': self.voice.get_text(),
            'api_token': self.api_token.get_text(),
            'folder_id': self.folder_id.get_text(),
            'lang': self.lang.get_text(),
            'root_folder': self.root_folder.get_filename()
        }
        Settings.store_settings(settings_data)
        self._reload_settings()

    def on_synthesize_clicked(self, obj, data=None):
        if self.settings_data is not None:
            self.settings_data['novel_name'] = self.novel_name.get_text()
            self.settings_data['part_name'] = self.part_name.get_text()

            if not (self.settings_data['novel_name'] or self.settings_data['part_name']):
                return

            if self.tts is None:
                self.tts = YandexTTS(**self.settings_data)
            else:
                self.tts.set_novel_name(self._slugify(self.novel_name.get_text()))
                self.tts.set_part_name(self._slugify(self.part_name.get_text()))

            text = self.text.get_text(
                self.text.get_start_iter(),
                self.text.get_end_iter(),
                include_hidden_chars=True
            ).strip(
                '\n'
            ).replace(
                '*', ''
            ).replace(
                '[', '"'
            ).replace(
                ']', '"'
            ).replace(
                '<', '"'
            ).replace(
                '>', '"'
            )

            self.tts.download_synthesis(text)
            self.load_playlist(self.novel_name.get_text())

    def clear_ui_playlist(self):
        for item in self.playlist.get_children():
            item.destroy()

    def get_current_position(self):
        if self.player.get_state(0).state in [Gst.State.PLAYING, Gst.State.PAUSED]:
            return self.player.query_position(Gst.Format.TIME).cur
        return 0

    def get_current_duration(self):
        return self.player.query_duration(Gst.Format.TIME).duration

    def on_play_button_clicked(self, obj=None, data=None):
        self.player.set_state(Gst.State.PLAYING)

        while self.player.get_state(0).state != Gst.State.PLAYING:
            continue

        duration = self.get_current_duration()
        self.progress_bar.set_range(0, duration)

    def on_pause_button_clicked(self, obj=None, data=None):
        self.player.set_state(Gst.State.PAUSED)

    def on_stop_button_clicked(self, obj=None, data=None):
        self.player.set_state(Gst.State.NULL)

    def go_to(self, to):
        self.on_stop_button_clicked()

        key_list = list(self.playlist_dict.keys())
        current_index = key_list.index(self.playing_now.get_text())
        index = 0

        if to == 'prev':
            index = current_index - 1
            if abs(index) > len(key_list) - 1:
                index = -1

        if to == 'next':
            index = current_index + 1
            if abs(index) > len(key_list) - 1:
                index = 0

        self.player.props.uri = self.playlist_dict[key_list[index]]
        self.on_play_button_clicked()
        self.playing_now.set_text(key_list[index])

    def on_prev_button_clicked(self, obj, data=None):
        self.go_to('prev')

    def on_next_button_clicked(self, obj, data=None):
        self.go_to('next')

    def on_volume_button_value_changed(self, obj, data=None):
        self.player.props.volume = data

    def play_selected(self, obj, data=None):
        self.playing_now.set_text(obj.get_text())
        self.on_stop_button_clicked()
        self.player.props.uri = self.playlist_dict[obj.get_text()]
        self.on_play_button_clicked()

    def load_playlist(self, novel_name):
        if not novel_name or self.settings_data is None:
            return

        novel_dir = os.path.join(self.settings_data['root_folder'], novel_name)
        files_list = sorted(os.listdir(novel_dir))

        if len(files_list) == 0:
            self.clear_ui_playlist()
            return

        self.clear_ui_playlist()

        playlist = OrderedDict()

        for file_name in files_list:
            playlist[file_name] = filepath2url(os.path.join(novel_dir, file_name))

            item = Gtk.Label()
            item.set_text(file_name)
            item.set_selectable(True)
            item.connect('button-press-event', self.play_selected)

            self.playlist.add(item)
            item.show()

        self.playlist_dict = playlist

    def on_window_destroy(self, obj, data=None):
        Gtk.main_quit()

    def on_window_show(self, obj, data=None):
        self._reload_settings()
        self._reload_novels_list()
        self.load_playlist(novel_name=self.novel_name.get_text())


if __name__ == "__main__":
    main = AwesomeReader() # create an instance of our class
    Gtk.main()
