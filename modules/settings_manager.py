import os
import json


class Settings:
    SETTINGS_FILE = 'settings.json'

    @classmethod
    def store_settings(cls, settings_data):
        with open(cls.SETTINGS_FILE, 'w') as settings_file:
            json.dump(settings_data, settings_file)

    @classmethod
    def load_settings(cls):
        if os.path.exists(cls.SETTINGS_FILE):
            with open(cls.SETTINGS_FILE) as settings_file:
                return json.load(settings_file)
