import os
import requests
from logging import getLogger
from slugify import slugify

logger = getLogger(__name__)


class InvalidResponseException(Exception):
    pass


class TextLimitExceeded(Exception):
    pass


class YandexTTS(object):
    IAM_TOKEN_URL = 'https://iam.api.cloud.yandex.net/iam/v1/tokens'
    TTS_URL = 'https://tts.api.cloud.yandex.net/speech/v1/tts:synthesize'
    TTS_TEXT_LIMIT = 5000

    def __init__(self, api_token, folder_id, root_folder, novel_name, part_name, lang, voice):
        logger.info('Initializing YandexTTS')
        self.api_token = api_token
        self.__iam_token = self.__refresh_iam_token(self.api_token)
        self.__folder_id = folder_id
        self.__root_folder = root_folder
        self.__novel_name = self.__slugify(novel_name)
        self.__part_name = self.__slugify(part_name)
        self.__lang = lang
        self.__voice = voice

    @staticmethod
    def __slugify(string):
        return slugify(string, to_lower=True, separator='_')

    @staticmethod
    def __check_response(response):
        if response.status_code == 200:
            return response

        raise InvalidResponseException(
            'Request failed with status code: {status_code}, response data {data}'.format(
                status_code=response.status_code,
                data=response.text
            )
        )

    def set_novel_name(self, newname):
        self.__novel_name = newname

    def set_part_name(self, newname):
        self.__part_name = newname

    def __refresh_iam_token(self, api_token):
        logger.info('Refreshing IAMToken...')
        response = requests.post(
            url=self.IAM_TOKEN_URL,
            json={'yandexPassportOauthToken': api_token}
        )
        self.__check_response(response)
        self.__iam_token = response.json()['iamToken']
        logger.info('IAMToken refreshed')

    def __validate_text_length(self, text):
        if len(text) > self.TTS_TEXT_LIMIT:
            raise TextLimitExceeded(
                'Maximum text length should be no more than {limit}'.format(
                    limit=self.TTS_TEXT_LIMIT
                )
            )

    def __synthesize(self, text):
        logger.info('Preparing request data for YandexTTS')
        self.__validate_text_length(text)

        if self.__iam_token is None:
            self.__refresh_iam_token(self.api_token)

        headers = {
            'Authorization': 'Bearer {iam_token}'.format(iam_token=self.__iam_token)
        }

        data = {
            'text': text,
            'lang': self.__lang,
            'folderId': self.__folder_id,
            'voice': self.__voice,
        }

        try:
            with requests.post(self.TTS_URL, headers=headers, data=data, stream=True) as response:
                self.__check_response(response)

                for chunk in response.iter_content():
                    yield chunk
        except InvalidResponseException:
            logger.info(
                'Bad response from YandexTTS. Status code: {status_code}, text: {response_text}'.format(
                    status_code=response.status_code,
                    response_text=response.text
                )
            )
            self.__refresh_iam_token(self.api_token)
            self.__synthesize(text)

    def __get_path_to_folder(self):
        folder_path = os.path.join(self.__root_folder, self.__novel_name)
        os.makedirs(folder_path, exist_ok=True)

        return folder_path

    def _prepare_text(self, text):
        if len(text) < self.TTS_TEXT_LIMIT:
            logger.info('Text length matches API limits. Sending as is...')
            yield text
        else:
            logger.info('Preparing text...')
            temp_chunk = ''
            lines = tuple(text.split('\n'))

            logger.info('Text length is bigger than API limit. Slicing...')
            for i in range(0, len(lines)):
                if len('\n'.join((temp_chunk, lines[i]))) >= self.TTS_TEXT_LIMIT:
                    chunk = temp_chunk
                    temp_chunk = lines[i]

                    yield chunk
                elif i == len(lines) - 1:
                    yield '\n'.join((temp_chunk, lines[i]))
                else:
                    temp_chunk = '\n'.join((temp_chunk, lines[i]))

    def download_synthesis(self, text):
        logger.info('Synthesis started!')

        part_num = 1
        path_to_folder = self.__get_path_to_folder()
        chunks = self._prepare_text(text)
        for chunk in chunks:
            file_name = '{name}__PART_{num}.ogg'.format(name=self.__part_name, num=part_num)
            file_path = os.path.join(path_to_folder, file_name)
            with open(file_path, 'wb') as audio_file:
                for audio_content in self.__synthesize(chunk):
                    audio_file.write(audio_content)

                logger.info('Saved part {part_num} as: {filepath}'.format(part_num=part_num, filepath=file_path))

            part_num += 1

        logger.info('Synthesis complete!')

        return path_to_folder
